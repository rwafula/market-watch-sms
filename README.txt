


Java Daemon Application
The SMSSender script can be copied to /etc/init.d/ and used in the normal
{start|stop|restart} manner on Linnux

INSTALLATION

1. Copy the dist folder to the location where you want the daemon to reside and
   give it a descriptive name.

2. Copy the file java_daemon_init to the folder /etc/init.d/ and make it
   executable. You can rename the file to the name of the application.

3. Edit the file SMSsender and provide correct values.

4. Run the daemon by issuing the command /etc/init.d/SMSSender start

5. Stop by issuing the command /etc/init.d/SMSSender stop

6. To install the application in all the run levels, run the command:

    (on debian based systems)
        sudo update-rc.d app-name defaults

    (on redhat based systems)
        /sbin/chkconfig --add app-name
        /sbin/chkconfig app-name on

