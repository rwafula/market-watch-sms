package utils;

import com.sun.org.apache.xalan.internal.xsltc.compiler.util.Type;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Loads system properties from a file.
 *
 * @author Reuben Paul wafula
 */
@SuppressWarnings({ "FinalClass", "ClassWithoutLogger" })
public final class Props {
    /**
     * The properties file.
     */
   private static final String PROPS_FILE = "SendSMS.conf";
    /**
     * A list of any errors that occurred while loading the properties.
     */
    private transient List<String> loadErrors;
    /**
     * Info log level. Default = INFO.
     */
    private transient String infoLogLevel = "INFO";
    /**
     * Error log level. Default = ERROR.
     */
    private transient String errorLogLevel = "ERROR";
    /**
     * Fatal log level. Default = FATAL.
     */
    private transient String fatalLogLevel = "FATAL";
    /**
     * Info log file name.
     */
    private transient String infoLogFile;
    /**
     * Error log file name.
     */
    private transient String errorLogFile;
    /**
     * Fatal log file name.
     */
    private transient String fatalLogFile;
    /**
     * Database connection pool name.
     */
    private transient String dbPoolName;
    /**
     * Database user name.
     */
    private transient String dbUserName;
    /**
     * Database password.
     */
    private transient String dbPassword;
    /**
     * Database host.
     */
    private transient String dbHost;
    /**
     * Database port.
     */
    private transient String dbPort;
    /**
     * Database name.
     */
    private transient String dbName;
    /**
     * Maximum database connections.
     */
    private transient int maxConnections;
    /**
     * Enumeration of values.
     */
    private transient Map<String, String> enumeratedKeyValues
            = new HashMap<String, String>(1);
    /**
     * Flag to ignore the queue size.
     */
    private transient boolean ignoreQueue = false;
    /**
     * The max queue size.
     */
    private transient int maxQueueSize;
    /**
     * The minimum credit.
     */
    private transient int minimumCredit;
    /**
     * Fetch bucket size.
     */
    private transient int bucketSize;
    /**
     * Daemon sleep time.
     */
    private transient int sleepTime = 1000;
    /**
     * No of threads that will be created in the thread pool to process
     * payments.
     */
    private transient int numOfThreads;
    /**
     * Current run ID. Will be incremented with each run.
     */
    private transient int currentRunID;
    /**
     * Max run ID. When hit currentRunID will be reset.
     */
    private transient int maxRunID;
    /**
     * The min run ID.
     */
    private transient int minRunID;

    /**
     * Infobi url XML POST
     */
    private transient String XMLSmsURL;
    
     /**
     * Infobi url PLAIN GET
     */
    private transient String GETSmsURL;
    
    /**
     * Infobi url JSOM POST
     */
    private transient String JSONSmsURL;
    
    /*
     * InfoBip api username
     */
    private transient String InfoBipUsername;
    
    /*
     * InfoBip api password
     */
    private transient String InfoBipPassword;
    
    private transient String InfoBipSender;
    
    private transient int route;
    
    //Params to connect to Africa's talking API
    private transient String africasTalkingKey;
    
    private transient String africasTalkingUsername;
    
    
    //GMS Model configs
    private transient String GSMid;
    private transient String GSMComPort;
    private transient int GSMBoudRate;
    private transient String GSMManufacturer; 
    private transient String GSMModel;
    private transient String GSMSIMPin;
    private transient String SMSCNumber;
    private transient boolean deleteMessageOnRead;
    
    //For incoming message here is the server to post
    
    private transient String serverURL;
    
    /**
     * Constructor.
     */
    public Props() {
        loadErrors = new ArrayList<String>(0);
        loadProperties(PROPS_FILE);
    }

    /**
     * Load system properties.
     *
     * @param propsFile the system properties xml file
     */
    @SuppressWarnings({ "UseOfSystemOutOrSystemErr", "unchecked" })
    private void loadProperties(final String propsFile) {
        FileInputStream propsStream = null;
        Properties props;

        try {
            props = new Properties();
            File base = new File(Props.class.getProtectionDomain().getCodeSource().getLocation().toURI()).getParentFile();
            
            propsStream = new FileInputStream(new File(base, propsFile));
            props.load(propsStream);

            String error1 = "ERROR: %s is <= 0 or may not have been set";
            String error2 = "ERROR: %s may not have been set";

            // Extract the values from the configuration file
            infoLogLevel = props.getProperty("InfoLogLevel");
            if (infoLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "InfoLogLevel"));
            }

            errorLogLevel = props.getProperty("ErrorLogLevel");
            if (errorLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "ErrorLogLevel"));
            }

            fatalLogLevel = props.getProperty("FatalLogLevel");
            if (fatalLogLevel.isEmpty()) {
                loadErrors.add(String.format(error2, "FatalLogLevel"));
            }
            
            XMLSmsURL = props.getProperty("XMLSmsURL");
            if (getXMLSmsURL().isEmpty()) {
                loadErrors.add(String.format(error2, "XMLSmsURL"));
            }
            
            GETSmsURL = props.getProperty("GETSmsURL");
            if (getGETSmsURL().isEmpty()) {
                loadErrors.add(String.format(error2, "GETSmsURL"));
            }
            
            JSONSmsURL = props.getProperty("JSONSmsURL");
            if (getJSONSmsURL().isEmpty()) {
                loadErrors.add(String.format(error2, "JSONSmsURL"));
            }
            
            
            InfoBipUsername = props.getProperty("InfoBipUsername");
            if (getInfoBipUsername().isEmpty()) {
                loadErrors.add(String.format(error2, "InfoBipUsername"));
            }
            
            InfoBipPassword = props.getProperty("InfoBipPassword");
            if (getInfoBipPassword().isEmpty()) {
                loadErrors.add(String.format(error2, "InfoBipPassword"));
            }
            
            InfoBipSender = props.getProperty("InfoBipSender");
            if (getInfoBipSender().isEmpty()) {
                loadErrors.add(String.format(error2, "InfoBipSender"));
            }
            

            infoLogFile = props.getProperty("InfoLogFile");
            if (infoLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "InfoLogFile"));
            }

            errorLogFile = props.getProperty("ErrorLogFile");
            if (errorLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "ErrorLogFile"));
            }

            fatalLogFile = props.getProperty("FatalLogFile");
            if (fatalLogFile.isEmpty()) {
                loadErrors.add(String.format(error2, "FatalLogFile"));
            }

            dbPoolName = props.getProperty("DbPoolName");
            if (dbPoolName.isEmpty()) {
                loadErrors.add(String.format(error2, "DbPoolName"));
            }

            dbUserName = props.getProperty("DbUserName");
            if (dbUserName.isEmpty()) {
                loadErrors.add(String.format(error2, "DbUserName"));
            }

            dbPassword = props.getProperty("DbPassword");
            if (dbPassword.isEmpty()) {
                loadErrors.add(String.format(error2, "DbPassword"));
            }

            dbHost = props.getProperty("DbHost");
            if (dbHost.isEmpty()) {
                loadErrors.add(String.format(error2, "DbHost"));
            }

            dbPort = props.getProperty("DbPort");
            if (dbPort.isEmpty()) {
                loadErrors.add(String.format(error2, "DbPort"));
            }

            dbName = props.getProperty("DbName");
            if (dbName.isEmpty()) {
                loadErrors.add(String.format(error2, "DbName"));
            }
            
            
            africasTalkingUsername = props.getProperty("AfricasTalkingUsername");
            if (getAfricasTalkingUsername().isEmpty()) {
                loadErrors.add(String.format(error2, "AfricasTalkingUsername"));
            }
            
            
            africasTalkingKey = props.getProperty("AfricasTalkingKey");
            if (getAfricasTalkingKey().isEmpty()) {
                loadErrors.add(String.format(error2, "AfricasTalkingUsername"));
            }
            
            

            String maxConns = props.getProperty("MaximumConnections");
            if (maxConns.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "MaximumConnections"));
            } else {
                maxConnections = Integer.parseInt(maxConns);
                if (maxConnections <= 0) {
                    loadErrors.add(String.format(error1,
                            "MaximumConnections"));
                }
            }

            for (Enumeration enumKeys = props.propertyNames();
                    enumKeys.hasMoreElements();) {
                String key = enumKeys.nextElement().toString();
                String value = props.getProperty(key);
                enumeratedKeyValues.put(key, value);
            }

            ignoreQueue = Boolean.parseBoolean(
                    props.getProperty("IgnoreQueue"));

            String maxQueue = props.getProperty("QueueThreshold");
            if (maxQueue.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "QueueThreshold"));
            } else {
                maxQueueSize = Integer.parseInt(maxQueue);
                if (maxQueueSize < 0) {
                    loadErrors.add(String.format(error1,
                            "QueueThreshold"));
                }
            }

            String minCredit = props.getProperty("MinimumCredit");
            if (minCredit.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "MinimumCredit"));
            } else {
                minimumCredit = Integer.parseInt(minCredit);
                if (minimumCredit < 0) {
                    loadErrors.add(String.format(error1,
                            "MinimumCredit"));
                }
            }
            
            String routeString = props.getProperty("Route");
            if (routeString.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "Route"));
            } else {
                route = Integer.parseInt(routeString);
                if (getRoute() <= 0) {
                    loadErrors.add(String.format(error1,
                            "Route"));
                }
            }

            String bucket = props.getProperty("BucketSize");
            if (bucket.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "BucketSize"));
            } else {
                bucketSize = Integer.parseInt(bucket);
                if (bucketSize < 0) {
                    loadErrors.add(String.format(error1,
                            "BucketSize"));
                }
            }

            String sleep = props.getProperty("SleepTime");
            if (sleep.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "SleepTime"));
            } else {
                sleepTime = Integer.parseInt(sleep);
                if (sleepTime < 0) {
                    loadErrors.add(String.format(error1,
                            "SleepTime"));
                }
            }

            String noc = props.getProperty("NumberOfThreads");
            if (noc.isEmpty()) {
                loadErrors.add(String.format(error1, "NumberOfThreads"));
            } else {
                numOfThreads = Integer.parseInt(noc);
                if (numOfThreads <= 0) {
                    loadErrors.add(String.format(error1,
                            "NumberOfThreads"));
                }
            }

            String currentRun = props.getProperty("CurrentRunAtStartup");
            if (currentRun.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "CurrentRunAtStartup"));
            } else {
                currentRunID = Integer.parseInt(currentRun);
                if (currentRunID < 0) {
                    loadErrors.add(String.format(error1,
                            "CurrentRunAtStartup"));
                }
            }

            String maxRun = props.getProperty("MaxRunID");
            if (maxRun.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "MaxRunID"));
            } else {
                maxRunID = Integer.parseInt(maxRun);
                if (maxRunID < 0) {
                    loadErrors.add(String.format(error1,
                            "MaxRunID"));
                }
            }

            String minRun = props.getProperty("MinRunID");
            if (minRun.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "MinRunID"));
            } else {
                minRunID = Integer.parseInt(minRun);
                if (minRunID < 0) {
                    loadErrors.add(String.format(error1,
                            "MinRunID"));
                }
            }
            
            
            
            GSMid = props.getProperty("GSMid");
            if (GSMid.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "GSMid"));
            } 
            
            GSMComPort = props.getProperty("GSMComPort");
            if (GSMComPort.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "GSMComPort"));
            } 
            
            
            String boudRate = props.getProperty("GSMBoudRate");
            if (boudRate.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "GSMBoudRate"));
            } else {
                try{
                    GSMBoudRate = Integer.parseInt(boudRate);
                }catch(NumberFormatException e){
                    loadErrors.add(String.format(error1,
                        "Invalid Boud Rate"));
                }
                
            }
            
            GSMManufacturer = props.getProperty("GSMManufacturer");
            if (GSMManufacturer.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "GSMManufacturer"));
            } 
            
            
            GSMModel = props.getProperty("GSMModel");
            if (GSMModel.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "GSMModel"));
            } 
            
            
            GSMSIMPin = props.getProperty("GSMSIMPin");
            if (GSMSIMPin.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "GSMSIMPin"));
            } 
            
            SMSCNumber = props.getProperty("SMSCNumber");
            if (SMSCNumber.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "SMSCNumber"));
            }
            
            serverURL = props.getProperty("ServerURL");
            if (serverURL.isEmpty()) {
                loadErrors.add(String.format(error1,
                        "serverURL"));
            }
            
            String _deleteMessageOnRead = props.getProperty("DeleteMessageOnRead");
            if (_deleteMessageOnRead.isEmpty() && getRoute() ==3) {
                loadErrors.add(String.format(error1,
                        "DeleteMessageOnRead"));
            }else{
                try{
                deleteMessageOnRead = Boolean.valueOf(_deleteMessageOnRead);
                }catch(Exception ex){
                    loadErrors.add(String.format(error1,
                        "INVALID DeleteMessageOnRead"));
                
                }
            }
            
            

            propsStream.close();
        } catch (NumberFormatException ne) {
            System.err.println("Exiting. String value found, Integer is "
                    + "required: " + ne.getMessage());

            try {
                propsStream.close();
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        } catch(URISyntaxException uis){
             System.err.println("Exiting. Could not find the properties file: "
                    + uis.getMessage());

        }catch (FileNotFoundException ne) {
            System.err.println("Exiting. Could not find the properties file: "
                    + ne.getMessage());

            try {
                propsStream.close();
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        } catch (IOException ioe) {
            System.err.println("Exiting. Failed to load system properties: "
                    + ioe.getMessage());

            try {
                propsStream.close();
            } catch (IOException ex) {
                System.err.println("Failed to close the properties file: "
                        + ex.getMessage());
            }

            System.exit(1);
        }
    }

    /**
     * A list of any errors that occurred while loading the properties.
     *
     * @return the loadErrors
     */
    public List<String> getLoadErrors() {
        return Collections.unmodifiableList(loadErrors);
    }

    /**
     * Info log level. Default = INFO.
     *
     * @return the infoLogLevel
     */
    public String getInfoLogLevel() {
        return infoLogLevel;
    }

    /**
     * Error log level. Default = ERROR.
     *
     * @return the errorLogLevel
     */
    public String getErrorLogLevel() {
        return errorLogLevel;
    }

    /**
     * Fatal log level. Default = FATAL.
     *
     * @return the fatalLogLevel
     */
    public String getFatalLogLevel() {
        return fatalLogLevel;
    }

    /**
     * Info log file name.
     *
     * @return the infoLogFile
     */
    public String getInfoLogFile() {
        return infoLogFile;
    }

    /**
     * Error log file name.
     *
     * @return the errorLogFile
     */
    public String getErrorLogFile() {
        return errorLogFile;
    }

    /**
     * Fatal log file name.
     *
     * @return the fatalLogFile
     */
    public String getFatalLogFile() {
        return fatalLogFile;
    }

    /**
     * Contains the name of the database pool.
     *
     * @return the name of the database pool
     */
    public String getDbPoolName() {
        return dbPoolName;
    }

    /**
     * Contains the name of the database user.
     *
     * @return the name of the database user
     */
    public String getDbUserName() {
        return dbUserName;
    }

    /**
     * Contains the name of the database password.
     *
     * @return the name of the database password
     */
    public String getDbPassword() {
        return dbPassword;
    }

    /**
     * Contains the name of the database host.
     *
     * @return the name of the database host
     */
    public String getDbHost() {
        return dbHost;
    }

    /**
     * Contains the name of the database port.
     *
     * @return the name of the database port
     */
    public String getDbPort() {
        return dbPort;
    }

    /**
     * Contains the name of the database.
     *
     * @return the name of the database
     */
    public String getDbName() {
        return dbName;
    }

    /**
     * Gets the maximum number of database connections.
     *
     * @return the maximum number of database connections
     */
    public int getMaxConnections() {
        return maxConnections;
    }

    /**
     * Gets the enumerated keys.
     *
     * @return the enumerated keys
     */
    public Map<String, String> getEnumeratedKeyValues() {
        return Collections.unmodifiableMap(enumeratedKeyValues);
    }

    /**
     * Gets the ignore queue value.
     *
     * @return the ignore queue value
     */
    public boolean getIgnoreQueue() {
        return ignoreQueue;
    }

    /**
     * Gets the max queue size.
     *
     * @return the max queue size
     */
    public int getMaxQueueSize() {
        return maxQueueSize;
    }

    /**
     * Gets the minimum credit.
     *
     * @return the minimum credit
     */
    public int getMinimumCredit() {
        return minimumCredit;
    }

    /**
     * Gets the bucket size.
     *
     * @return the bucket size
     */
    public int getBucketSize() {
        return bucketSize;
    }

    /**
     * Gets the sleep time.
     *
     * @return the sleep time
     */
    public int getSleepTime() {
        return sleepTime;
    }

    /**
     * No of threads that will be created in the thread pool to process
     * payments.
     *
     * @return the numOfChildren
     */
    public int getNumOfThreads() {
        return numOfThreads;
    }

    /**
     * Gets the current run ID.
     *
     * @return the current run ID
     */
    public int getCurrentRunID() {
        return currentRunID;
    }

    /**
     * Gets the max run ID.
     *
     * @return the max run ID
     */
    public int getMaxRunID() {
        return maxRunID;
    }

    /**
     * Gets the min run ID.
     *
     * @return the min run ID
     */
    public int getMinRunID() {
        return minRunID;
    }

    /**
     * @return the XMLSmsURL
     */
    public String getXMLSmsURL() {
        return XMLSmsURL;
    }

    /**
     * @return the GETSmsURL
     */
    public String getGETSmsURL() {
        return GETSmsURL;
    }

    /**
     * @return the JSONSmsURL
     */
    public String getJSONSmsURL() {
        return JSONSmsURL;
    }

    /**
     * @return the InfoBipUsername
     */
    public String getInfoBipUsername() {
        return InfoBipUsername;
    }

    /**
     * @return the InfoBipPassword
     */
    public String getInfoBipPassword() {
        return InfoBipPassword;
    }

    /**
     * @return the InfoBipSender
     */
    public String getInfoBipSender() {
        return InfoBipSender;
    }

    /**
     * @return the route
     */
    public int getRoute() {
        return route;
    }

    /**
     * @return the africasTalkingKey
     */
    public String getAfricasTalkingKey() {
        return africasTalkingKey;
    }

    /**
     * @return the africasTalkingUsername
     */
    public String getAfricasTalkingUsername() {
        return africasTalkingUsername;
    }

    /**
     * @return the GSMid
     */
    public String getGSMid() {
        return GSMid;
    }

    /**
     * @return the GSMComPort
     */
    public String getGSMComPort() {
        return GSMComPort;
    }

    /**
     * @return the GSMBoudRate
     */
    public int getGSMBoudRate() {
        return GSMBoudRate;
    }

    /**
     * @return the GSMManufacturer
     */
    public String getGSMManufacturer() {
        return GSMManufacturer;
    }

    /**
     * @return the GSMModel
     */
    public String getGSMModel() {
        return GSMModel;
    }

    public String getSIMPin(){
        return GSMSIMPin;
    }
    
    
    public String getSMSCNumber(){
        return SMSCNumber;
    }
    
    public String getServerURL(){
        return this.serverURL;
    }
    
    public boolean getDeleteMessageOnRead(){
        return this.deleteMessageOnRead;
    }
}
