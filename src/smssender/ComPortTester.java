/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smssender;

/**
 *
 * @author rube
 */

import java.util.Enumeration;
import java.util.Formatter;

import org.apache.log4j.Logger;
import org.smslib.helper.CommPortIdentifier;

public class ComPortTester {
    
   private static final String _NO_DEVICE_FOUND = "  no device found";

   private final static Formatter _formatter = new Formatter(System.out);

   private static Logger log = Logger.getLogger(ComPortTester.class);

   static CommPortIdentifier portId;

   static Enumeration<CommPortIdentifier> portList;

   static int bauds[] = { 9600, 14400, 19200, 28800, 33600, 38400, 56000,
                          57600, 115200 };

   public static final String MAINCLASS = "org.smslib.Service";

   public ComPortTester() throws Exception {
      Class.forName(MAINCLASS);
   }

   /**
    * Wrapper around {@link CommPortIdentifier#getPortIdentifiers()} to be
    * avoid unchecked warnings.
    */
   private static Enumeration<CommPortIdentifier> getCleanPortIdentifiers() {
      return CommPortIdentifier.getPortIdentifiers();
   }

   public void testAndQualifyPort() throws Exception {
     
      log.debug("\nSearching for devices...");
      System.out.println("\nSearching for devices...");
      portList = getCleanPortIdentifiers();
      System.out.println("Port List "+portList + "and is Empty "+portList.hasMoreElements());
      while (portList.hasMoreElements()) {
        
          
         portId = portList.nextElement();
          System.out.println("Port ID "+portId);
         if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
            _formatter.format("%nFound port: %-5s%n", portId.getName());
            try {
               if(!portId.getName().equals("")){
              
                  System.out.println("Another Port: .."+portId.getName());
               }else{
                   System.out.println("Found NULL port name");
               }    
            } catch (final Exception e) {
               log.debug(" Modem error occured -",e);
               System.out.println("Model Error");
            }
         }else{
             System.out.println("This is not a serial port");
         }
      }
      log.debug("\nTest complete.");
      System.out.println("Test Complete");
      
   }

   public static void main(String[]args){
      try{
          System.err.println("Starting port check ...");
        ComPortTester tester = new ComPortTester(); 
        tester.testAndQualifyPort();
        System.err.println("Ending port check ...");
      }catch(Exception e){
         e.printStackTrace();
      }
   }
}
