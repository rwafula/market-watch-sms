package smssender;

/**
 *
 * @author rube
 */
//
// This application shows you the basic procedure needed for reading
// SMS messages from your GSM modem, in synchronous mode.
//
// Operation description:
// The application setup the necessary objects and connects to the phone.
// As a first step, it reads all messages found in the phone.
// Then, it goes to sleep, allowing the asynchronous callback handlers to
// be called. Furthermore, for callback demonstration purposes, it responds
// to each received message with a "Got It!" reply.
//
// Tasks:
// 1) Setup Service object.
// 2) Setup one or more Gateway objects.
// 3) Attach Gateway objects to Service object.
// 4) Setup callback notifications.
// 5) Run
import db.MySQL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import org.smslib.AGateway;
import org.smslib.AGateway.GatewayStatuses;
import org.smslib.AGateway.Protocols;
import org.smslib.ICallNotification;
import org.smslib.IGatewayStatusNotification;
import org.smslib.IInboundMessageNotification;
import org.smslib.InboundMessage;
import org.smslib.InboundMessage.MessageClasses;
import org.smslib.Library;
import org.smslib.Message.MessageTypes;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import utils.Logging;
import utils.Props;

public class ReadGSMSMS {

    private final String GSMid;
    private final String GSMComPort;
    private final int GSMBoudRate;
    private final String GSMManufacturer;
    private final String GSMModel;
    private final String SIMPin;
    private final String SMSCNumber;
    private final String url;
    private Service service;

    private String from;
    private String message;
    private transient Logging logger;
    boolean deleteMessageOnRead;
    
    private MySQL mysql;

    public ReadGSMSMS(MySQL mysqlPool, Props props, Logging logger) {
        this.GSMid = props.getGSMid();
        this.GSMComPort = props.getGSMComPort();
        this.GSMBoudRate = props.getGSMBoudRate();
        this.GSMManufacturer = props.getGSMManufacturer();
        this.GSMModel = props.getGSMModel();
        this.SIMPin = props.getSIMPin();
        this.SMSCNumber = props.getSMSCNumber();
        this.logger = logger;
        this.url = props.getServerURL();
        this.mysql = mysqlPool;
        this.deleteMessageOnRead = props.getDeleteMessageOnRead();
        
        this.logger.info(Thread.currentThread().getName() + " SendGSMSMS ID " + this.GSMid + " ..Port: " + this.GSMComPort
                + "Rate " + this.GSMBoudRate + " Manu " + this.GSMManufacturer
                + " Model " + this.GSMModel + " SIM " + this.SIMPin);
    }

    public void doIt() throws Exception {
        this.logger.info(Thread.currentThread().getName() + " Calling do receive messages ");
        // Define a list which will hold the read messages.
        List<InboundMessage> msgList;
        try {
            // Create the notification callback method for inbound & status report
            // messages.
            InboundNotification inboundNotification = new InboundNotification();
            // Create the notification callback method for inbound voice calls.
            CallNotification callNotification = new CallNotification();
            //Create the notification callback method for gateway statuses.
            GatewayStatusNotification statusNotification = new GatewayStatusNotification();
        
            this.logger.info(Thread.currentThread().getName() + "Read messages from a serial gsm modem.");
            this.logger.info(Thread.currentThread().getName() + " " + Library.getLibraryDescription());
            this.logger.info(Thread.currentThread().getName() + " Version: " + Library.getLibraryVersion());
            // Create the Gateway representing the serial GSM modem.
            SerialModemGateway gateway = new SerialModemGateway(GSMid, GSMComPort,
                    GSMBoudRate, GSMManufacturer, GSMModel);
            // Set the modem protocol to PDU (alternative is TEXT). PDU is the default, anyway...
            gateway.setProtocol(Protocols.PDU);
            // Do we want the Gateway to be used for Inbound messages?
            gateway.setInbound(true);
            // Do we want the Gateway to be used for Outbound messages?
            gateway.setOutbound(true);
            // Let SMSLib know which is the SIM PIN.
            gateway.setSimPin(SIMPin);
            // Set up the notification methods.
            service = new Service();

            service.setInboundNotification(inboundNotification);
            service.setCallNotification(callNotification);
            service.setGatewayStatusNotification(statusNotification);
            // Add the Gateway to the Service object.
            service.addGateway(gateway);
            // Similarly, you may define as many Gateway objects, representing
            // various GSM modems, add them in the Service object and control all of them.
            // Start! (i.e. connect to all defined Gateways)
            service.startService();            
            this.logger.info(Thread.currentThread().getName() + "Service started ");
            // Printout some general information about the modem.
            System.out.println();
            this.logger.info(Thread.currentThread().getName() + " Modem Information:");
            this.logger.info(Thread.currentThread().getName() + "  Manufacturer: " + gateway.getManufacturer());
            this.logger.info(Thread.currentThread().getName() + "  Model: " + gateway.getModel());
            this.logger.info(Thread.currentThread().getName() + "  Serial No: " + gateway.getSerialNo());
            this.logger.info(Thread.currentThread().getName() + "  SIM IMSI: " + gateway.getImsi());
            this.logger.info(Thread.currentThread().getName() + "  Signal Level: " + gateway.getSignalLevel() + " dBm");
            this.logger.info(Thread.currentThread().getName() + "  Battery Level: " + gateway.getBatteryLevel() + "%");
            this.logger.info(Thread.currentThread().getName() + " ");
            // Read Messages. The reading is done via the Service object and
            // affects all Gateway objects defined. This can also be more directed to a specific
            // Gateway - look the JavaDocs for information on the Service method calls.
            msgList = new ArrayList<InboundMessage>();
            service.readMessages(msgList, MessageClasses.ALL);
            this.logger.info(Thread.currentThread().getName() + "Reading message ... Found :"+msgList.size());
            for (InboundMessage msg : msgList) {

                this.logger.info(Thread.currentThread().getName() + "Reading message ... list inside");
                this.logger.info(Thread.currentThread().getName() + msg);
                this.message = msg.getText();
                this.from = msg.getOriginator();
                
                //adding date received as captured on the phone
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                String dateReceived =  df.format(msg.getDate());
                
                saveReceivedMessage(this.from, this.message, dateReceived);
                
               
                //Delete message from phone
                if(deleteMessageOnRead){
                     this.logger.info(Thread.currentThread().getName() + "Message processed success ... "
                        + " deleting from gateway ...");
                    gateway.deleteMessage(msg);
                    this.logger.info(Thread.currentThread().getName() + "Message deleted success ... "
                        + " returning ...");
                }
                
                 this.logger.info(Thread.currentThread().getName() + "Message processed success ...");
                
            }

        } catch (Exception e) {
            e.printStackTrace();
            this.logger.info(Thread.currentThread().getName() + "Error reading message ..." + e.getMessage());

        }catch (java.lang.UnsatisfiedLinkError e) {
            e.printStackTrace();
            this.logger.info(Thread.currentThread().getName() + "Error UnsatisfiedLinkError ..." + e.getMessage());

        }finally {
            try{
                service.stopService();
            }catch(Exception ex){
                ex.printStackTrace();
                this.logger.info(Thread.currentThread().getName() + "Shot my "
                        + "feet trying to stop non service. ."+ ex.getMessage());
            }
        }
    }

    public class InboundNotification implements IInboundMessageNotification {

        public void process(AGateway gateway, MessageTypes msgType, InboundMessage msg) {
            if (msgType == MessageTypes.INBOUND) {
                logger.info(Thread.currentThread().getName() +">>> New Inbound message detected from Gateway: " + gateway.getGatewayId());
                try{
                    //saveReceivedMessage(msg.getOriginator(), msg.getText());
                }catch(Exception ex){
                    logger.error("Error processing inbound message "+ex.getMessage());
                }
                
            } else if (msgType == MessageTypes.STATUSREPORT) {
                logger.info(Thread.currentThread().getName() +">>> New Inbound Status Report message detected from Gateway: " + gateway.getGatewayId());
            }
            System.out.println(msg);
        }

        @Override
        public void process(String string, MessageTypes msgType, InboundMessage msg) {

             if (msgType == MessageTypes.INBOUND) {
                logger.info(Thread.currentThread().getName() +">>> New Inbound message detected from Gateway: " + string);
                try{
                    //saveReceivedMessage(msg.getOriginator(), msg.getText());
                }catch(Exception ex){
                    logger.error("Error processing inbound message "+ex.getMessage());
                }
                
            } else if (msgType == MessageTypes.STATUSREPORT) {
                logger.info(Thread.currentThread().getName() +">>> New Inbound Status Report message detected from Gateway: " + string);
            }
            System.out.println(msg);
        }
    }

    public class CallNotification implements ICallNotification {

        @Override
        public void process(String gateway, String callerId) {
            System.out.println(">>> New call detected from Gateway: " + gateway + " : " + callerId);
        }
    }

    public class GatewayStatusNotification implements IGatewayStatusNotification {

        @Override
        public void process(String gateway, GatewayStatuses oldStatus, GatewayStatuses newStatus) {
            System.out.println(">>> Gateway Status change for " + gateway + ", OLD: " + oldStatus + " -> NEW: " + newStatus);
        }
    }
    
    
    public boolean messageAlreadySaved(String from, String message){
        boolean isDuplicate = false;
        String insertCheckMessage = "select id from in_message where from_address = '"+from+"' "
                + " and message = '"+message+"'";
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        int insertId = 0;
        
        try {
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(insertCheckMessage);
            if(rs.next()){
                //insertId = rs.getInt(1);
                logger.info("Duplicate Message will skip save message to inbox \n "
                        + " "+message + " : from "+ from);
               isDuplicate = true; 
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | messageAlreadySaved --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | messageAlreadySaved --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | messageAlreadySaved --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
        return isDuplicate;
    }

    // HTTP POST request
    private void saveReceivedMessage(String _from, String _message, String dateReceived) throws Exception {
       
        //Abandon if this is a duplicate message
        if(messageAlreadySaved(_from, _message)){
            return;
        }
        
        String insertMessage = "insert into in_message ("
                + " from_address, to_address, message_id, message, date_received )"
                + " values ('"+_from+"', 'WFP', '0', '"+_message+"', '"+dateReceived+"')";
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        int insertId = 0;
        
        try {
            logger.info("SaveReceivedMessageQuery :"+insertMessage);
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            stmt.executeUpdate(insertMessage, Statement.RETURN_GENERATED_KEYS);
            rs = stmt.getGeneratedKeys();
            if(rs.next()){
                insertId = rs.getInt(1);
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | saveReceivedMessage --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | saveReceivedMessage --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | saveReceivedMessage --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
        processResponseMessage(insertId);
        
    }
    
    public String getItemMeasure(String text){
        String queryMessage = "select i.id from item_measure i where i.name like '%"+text+"%' limit 1";
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String measureName = "";

        try {
             logger.info("getItemMeasureQuery :"+queryMessage);
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(queryMessage);
            
            if(rs.next()){
                measureName=rs.getString(1);
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | getItemName --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getItemName --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getItemName --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
       return measureName;
    }
    
    
    public String getPrice(String item, String measure, String location){
        String queryMessage = "select ip.price, ip.date_created, l.market, i.name as item_name,"
                + " im.name as item_measure from item_price ip "
                + " inner join item i on i.id = ip.item_id "
                + " inner join item_measure im on im.id = ip.measure_id"
                + " inner join location l on l.id = ip.location_id "
                + " where "
                + "ip.item_id = '"+item+"' and ip.location_id = '"+location+"'"
                + " and ip.measure_id = '"+measure+"' order by ip.id desc limit 1";
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String responseMessage = "";

        try {
            logger.info("getpriceQuery :"+queryMessage);
            conn = mysql.getConnection();
            stmt = conn.createStatement();            
            rs = stmt.executeQuery(queryMessage);
            
            if(rs.next()){
                responseMessage = rs.getString("item_name") +
                        " in "
                        + rs.getString("item_measure")+
                        " selling for "
                        + rs.getString("price") +
                        " at "
                        + rs.getString("date_created");
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | getPrice --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getPrice --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getPrice --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
       return responseMessage;
    }
    
    
    public String getLocation(String text){
        String queryMessage = "select l.id from location l where l.market like '%"+text+"%' limit 1";
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String market = "";

        try {
             logger.info("getLocationQuery :"+queryMessage);
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(queryMessage);
            
            if(rs.next()){
                market=rs.getString(1);
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | getLocation --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getLocation --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getLocation --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
       return market;
    }
    
    public String getItemName(String text){
        String queryMessage = "select i.id from item i where i.name like '%"+text+"%' limit 1";
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String itemName = "";

        try {
            logger.info("getItemQuery :"+queryMessage);
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            rs = stmt.executeQuery(queryMessage);
            
            if(rs.next()){
                itemName=rs.getString(1);
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | getItemName --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getItemName --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | getItemName --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
       return itemName;
    }
    
    public void processResponseMessage(int insertId){
        //Split message on space
        String[] splitMessages = this.message.split("\\s+");
        String responseMessage = "";
        String item="", measure ="", location = "";
        //We are looking item, measure and location
        // will assume Item, measure, location in that order. The SMS must start with the
        //word price followed by item measure and location
        String firstWord = splitMessages[0];
        if (!firstWord.equalsIgnoreCase("price")){
                //responseMessage = "Invalid message, Please send SMS starting with word "
                //        + "Price followed by Item, measure and location in that order";
                
                //We don't proceed when message doesn't start with word price we simply skipp
                //and log so
                logger.info("Found message NOT STARTING WITH THE WORD 'Price' Skippig "
                + " to process response ... \n Here is the message " + this.message);
                return;
        }
        
        
    
        if(responseMessage.equals("")){
            
            //Skipping index 0 .. check
            for(int i =1; i <splitMessages.length; i++){
                
                if (item.equals("")) {
                    item = getItemName(splitMessages[i]); 
                    if (!item.equals("")) continue;
                }


                if (measure.equals("")) {
                    measure = getItemMeasure(splitMessages[i]); 
                    if (!measure.equals("")) continue;
                }

                 if (location.equals("")) {
                    location = getLocation(splitMessages[i]);                     
                }
            }

            if (item.equals("")|| location.equals("") ||measure.equals("")){
                logger.info("Failed to find one of Item : "+item + " Location :"+location+ " Measure "+measure);
                responseMessage = "Invalid message, We we not able to extract one of Item, "
                        + " measure or location from your message. Please check and retry";
            }else {
                logger.info("Found Item : "+item + " Location :"+location+ " Measure "+measure);
                responseMessage = getPrice(item, measure, location);
            }

            if(responseMessage.equals("")){
                responseMessage = "No approved price found for Item for provided criteria";
            }
        }
        
        
        String requestId = updatePriceRequest(item, measure, location, this.from);
        
        String insertSQl = "insert into out_message (message, from_address,"
                + " to_address, in_message_id, priority, delivered, date_send, "
                + " date_delivered ) values ('"+responseMessage+"', "
                + " 'WFP', '"+this.from+"', '"+insertId+"', 1, 0, now(), null )";
        
        logger.info("saving Message "+insertSQl);
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        try {
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            int affectedRows = stmt.executeUpdate(insertSQl);
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | processResponseMessage --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | processResponseMessage --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | processResponseMessage --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
        
        updatePriceResponse(requestId, responseMessage);
                
        
    }
    
    public String  updatePriceRequest(String itemId, String measureId, 
        String locationId, String fromAddress ){
        
        itemId = itemId.isEmpty()? "0": itemId;
        measureId = measureId.isEmpty()? "0": measureId;
        locationId = locationId.isEmpty()?"0":locationId;
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String requestId = "";
        
        String insertQuery = "insert into price_request (id, date_created,"
                + " user_id, country_id, organization_id, item_id, serviced, "
                + " date_serviced, location_id, measure_id ) values ("
                + " null, now(), "
                + " 1, 1, 1,"
                + " '"+itemId+"',"
                + " '"+1+"', "
                + " now() ,"
                + " '"+locationId+"',"
                + " '"+measureId+"' )"; 

        
        try {
            logger.info("updateRequestQuery: " +insertQuery); 
            conn = mysql.getConnection();
            stmt = conn.createStatement();
            stmt.executeUpdate(insertQuery, Statement.RETURN_GENERATED_KEYS);
            rs = stmt.getGeneratedKeys();
            if(rs.next()){
                requestId = rs.getString(1);
            }
            
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | updatePriceRequest --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | updatePriceRequest --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | updatePriceRequest --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
        return requestId;
    
    }
    
    
    public void  updatePriceResponse(String requestId, 
        String message ){
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        
        String insertQuery = "insert into price_response (id, request_id,"
                + " message, date_created ) values ("
                + " null, '"+requestId+"', "
                + " '"+message+"',  now() ) "; 

        
        try {
            logger.info("updatePriceResponseQuery: " +insertQuery); 
            conn = mysql.getConnection();
            stmt = conn.createStatement();
            int affectedRows = stmt.executeUpdate(insertQuery);
            
        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | updatePriceResponse --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | updatePriceResponse --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | updatePriceResponse --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
            
        }
        
    }

}
