package smssender;

import java.io.IOException;

import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.List;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import db.MySQL;

import utils.Logging;
import utils.BMConstants;
import utils.Props;

/**
 * This is the Daemon's main class. When loaded, it checks the database for any
 * requests. When a request is found, it queues the request to the job class
 * which sends it to EMG.
 */
@SuppressWarnings("FinalClass")
public final class SendSMSDaemon {

    /**
     * System properties class instance.
     */
    private transient Props props;
    /**
     * Log class instance.
     */
    private transient Logging logging;
    /**
     * Instance of the MySQL connection pool.
     */
    private transient MySQL mysql;
    /**
     * The current run ID.
     */
    private transient int currentRunID;
    /**
     * Send rules.
     */
    private transient String[][] allRules;
    /**
     * Flag to check if current pool is completed.
     */
    private transient boolean isCurrentPoolShutDown = false;
    
   

    /**
     * Constructor. Checks for any errors while loading system properties,
     * creates the thread pool and resets partially processed records.
     *
     * @param log the logger class used to log information and errors
     * @param properties the loaded system properties
     * @param mysqlConnection the MySQL connection pool
     */
    @SuppressWarnings("SleepWhileInLoop")
    public SendSMSDaemon(final Props properties, final Logging log,
            final MySQL mysqlConnection) {
        props = properties;
        logging = log;
        mysql = mysqlConnection;

        currentRunID = props.getCurrentRunID();

        // Get the list of errors found when loading system properties
        List<String> loadErrors = properties.getLoadErrors();
        int sz = loadErrors.size();

        if (sz > 0) {
            log.info("There were exactly " + sz
                    + " error(s) during the load operation...");

            for (String err : loadErrors) {
                log.fatal(err);
            }

            log.info("Unable to start daemon because " + sz
                    + " error(s) occured during load.");
            System.exit(1);
        } else {
            log.info("All the required properties were loaded successfully...");

            double pingTime = (double) (props.getSleepTime() / 1000);
            log.info("Checking whether the database is up and running... "
                    + "(Sending ping every " + pingTime + " sec(s)...)");

            while (true) {
                int state = pingDatabaseServer(props.getDbHost(),
                        props.getDbPort());

                if (state == BMConstants.PING_SUCCESS) {
                    log.info("Database is up and running...");
                    break;
                } else {
                    log.error("Unable to start daemon because the database is "
                            + "not responding. Retrying in 10 seconds...");
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException ex) {
                        log.error("Error while waiting for database: "
                                + ex.getMessage());
                    }
                }
            }
        }
    }

    /**
     * Creates a stream socket and connects it to the specified port number on
     * the named host.
     *
     * @param host the host
     * @param port the port
     *
     * @return the state of the ping
     */
    private int pingDatabaseServer(final String host, final String port) {
        int state;

        try {
            int portNumber = Integer.parseInt(port);
            Socket ps = new Socket(host, portNumber);

            /*
             * Same time we use to sleep is the same time we use for the ping
             * wait period.
             */
            ps.setSoTimeout(props.getSleepTime());

            if (ps.isConnected()) {
                state = BMConstants.PING_SUCCESS;
            } else {
                state = BMConstants.PING_FAILED;
            }

            ps.close();
        } catch (UnknownHostException ex) {
            state = BMConstants.PING_FAILED;
        } catch (SocketException ex) {
            state = BMConstants.PING_FAILED;
        } catch (IOException ex) {
            state = BMConstants.PING_FAILED;
        }

        return state;
    }

    /**
     * Makes the current thread sleep for the specified time.
     *
     * @param time the time to sleep
     */
    private void doWait(final long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException ex) {
            logging.error("Interrupted while sleeping: " + ex.getMessage());
        }
    }

    /**
     * Creates a simple Runnable that holds a Job object thats worked on by the
     * children threads.
     *
     * @param outboundID the outbound ID
     * @param sourceAddress the source address
     * @param destinationAddress the destination address
     * @param message the message being sent
     * @param connUrl the connection url
     * @param connectorIndex the connector index
     *
     * @return a runnable job task
     */
    private synchronized Runnable createTask(final String msisdn,
           final String messageContent, final int numberOfSends, final int messageID, final String sourceAddr) {
  
        logging.info(SendSMSDaemon.class.getName() + "| createTask(...) "
                + "for msisdn =  " + msisdn);
        return new SMSJob(mysql, logging, msisdn, messageContent, 
                numberOfSends, messageID, sourceAddr, props);
    }

    /**
     * The following method shuts down an ExecutorService in two phases, first
     * by calling shutdown to reject incoming tasks, and then calling
     * shutdownNow, if necessary, to cancel any lingering tasks (after 6
     * minutes).
     *
     * @param pool the executor service pool
     * @param runID the run ID used for the current pooled tasks
     */
    private void shutdownAndAwaitTermination(final ExecutorService pool) {
        logging.info("Executor pool  waiting for tasks to "
                + "complete");
        pool.shutdown(); // Disable new tasks from being submitted

        try {
            // Wait a while for existing tasks to terminate
            if (!pool.awaitTermination(6, TimeUnit.MINUTES)) {
                logging.error("Executor pool  terminated with "
                        + "tasks unfinished. Resetting unfinished tasks");
                pool.shutdownNow(); // Cancel currently executing tasks

                // Wait a while for tasks to respond to being cancelled
                if (!pool.awaitTermination(6, TimeUnit.MINUTES)) {
                    logging.error("Executor pool  terminated "
                            + "with tasks unfinished. Resetting unfinished "
                            + "tasks");

                }
            } else {
                logging.info("Executor pool  completed all "
                        + "tasks and has shut down");
            }
        } catch (InterruptedException ie) {
            logging.error("Executor pool ' shutdown error: "
                    + ie.getMessage());
            // (Re-)Cancel if current thread also interrupted
            pool.shutdownNow();
            // Preserve interrupt status
            Thread.currentThread().interrupt();
        }

        isCurrentPoolShutDown = true;
    }

    /**
     * Gets whether the current pool has been shut down.
     *
     * @return whether the current pool has been shut down
     */
    public boolean getIsCurrentPoolShutDown() {
        return isCurrentPoolShutDown;
    }

    
    //Read messages received by the modem ... and process requests
    public void readMessages(){
        logging.info("Checking messages from the MODEM");        
        
        try {
            ReadGSMSMS messageReader = new ReadGSMSMS(mysql, props, logging);
            logging.error("Calling message reader ");
            messageReader.doIt();
        }catch(Exception ex){
            logging.error("Problem Reading messages "+ ex.getMessage());
            ex.printStackTrace();
        
        }
            
    
    }
    

    /**
     * Process a bucket.
     * We check from db messages pending to be send and sent via infobip
     */
    private void sendMessages() {

        String query;

        // Fetch one bucket of message at time -- check config
        query = "select id, from_address, to_address, message, number_of_sends "
                + "from out_message where status = 0 "
                + " order by priority asc limit "+props.getBucketSize();

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = mysql.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);

            logging.info("sendMessages()  using select query: " + query);

            ExecutorService executor = Executors
                    .newFixedThreadPool(props.getNumOfThreads());

            isCurrentPoolShutDown = false;

            while(rs.next()) {
                //we got result set fetch contacts and insert into outbound
                String  message = rs.getString("message");
                String msisdn = rs.getString("to_address");
                int numberOfSends = rs.getInt("number_of_sends");
                int messageID = rs.getInt("id");
                String sourceAddr = rs.getString("from_address");
                //ResultSet subscriberRs = null;

                logging.info("Attempting to sent message \n " + message + 
                        " \n to phone number " + msisdn +" form  "+sourceAddr);

                    // Create a runnable task and submit it
                  Runnable task = createTask(msisdn, message, numberOfSends, messageID, sourceAddr);            

                    executor.execute(task);

                }

                rs.close();
                stmt.close();
                conn.close();
            
            shutdownAndAwaitTermination(executor);
        } catch (SQLException ex) {
            logging.error(SendSMSDaemon.class.getName()
                    + " | sendMessages --- SQL "
                    + " transactions for processing: " + ex.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    logging.fatal(SendSMSDaemon.class.getName()
                            + " | sendMessages --- Failed to close "
                            + "ResultSet object. Error: "
                            + ex.getMessage());
                }
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logging.fatal(SendSMSDaemon.class.getName()
                            + " | sendMessages --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logging.fatal(SendSMSDaemon.class.getName()
                            + " | sendMessages --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
        }
        
        isCurrentPoolShutDown = true;
    }

    /**
     * A better functional logic that ensures secure execution of fetch bucket
     * as well as detailed management of interrupted queries.
     */
    private void doWork() {
        
        sendMessages();
        
        //Route 3 - Modem read Messages as well
        if(props.getRoute() == 3){
            readMessages();
        
        }
    }

    /**
     * Processes messages.
     */
    public void processRequests() {
        int daemonState = BMConstants.DAEMON_RUNNING;
        logging.info("I am in my running state. Running_state id: "
                + BMConstants.DAEMON_RUNNING);

        int state = pingDatabaseServer(props.getDbHost(), props.getDbPort());

        if (state == BMConstants.PING_SUCCESS) {
            // Database is available, so allocate and fetch a bucket
            if (daemonState == BMConstants.DAEMON_RUNNING) {
                doWork();
            } else {
                logging.info(SendSMSDaemon.class.getName()
                        + "| Connection to the database was re-established. "
                        + "Restoring service...");

                doWait(props.getSleepTime());

                if (props.getCurrentRunID() > props.getMinRunID()) {
                    currentRunID = props.getMinRunID();
                    logging.info(SendSMSDaemon.class.getName()
                            + " | Performing system restore => resetting the "
                            + "Current_Run_ID to initial run_id [ "
                            + currentRunID + " ]...");
                }

                logging.info(SendSMSDaemon.class.getName()
                        + "| resuming daemon service...");
                daemonState = BMConstants.DAEMON_RUNNING;
                logging.info(SendSMSDaemon.class.getName()
                        + "| daemon service resumed successfully, working...");
            }
        } else {
            logging.fatal("The database server: " + props.getDbHost()
                    + " servicing on port: " + props.getDbPort()
                    + " appears to be down (internal function for "
                    + "pingDatabaseServer() returned a PING_FAILED status)");
            daemonState = BMConstants.DAEMON_INTERRUPTED;

            logging.info(SendSMSDaemon.class.getName()
                    + "| Connection to the database was interrupted, "
                    + "suspending service...");

            while (true) {
                // Enter a Suspended state
                int istate = pingDatabaseServer(props.getDbHost(),
                        props.getDbPort());

                if (istate == BMConstants.PING_SUCCESS) {
                    daemonState = BMConstants.DAEMON_RESUMING;
                    break;
                }

                logging.fatal(SendSMSDaemon.class.getName()
                        + "| Unable to connect to the database. Retrying in 10 "
                        + "seconds..");
                doWait(10000);
            }
        }
    }

    /**
     * Method <i>getCurrentRun</i> determines and returns the Current Run ID to
     * be used when allocating a bucket for the respective iteration.
     *
     * @return the Current processRequests ID
     */
    private int getCurrentRun() {
        /*
         * Check if the CURRENT_RUN_AT_STARTUP has exceeded the MAXMIMUM values.
         */
        if (currentRunID > props.getMaxRunID()) {
            currentRunID = props.getMinRunID();
        } else {
            currentRunID++;
        }

        return currentRunID;
    }
}
