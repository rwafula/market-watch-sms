package smssender;

import java.sql.SQLException;

import db.MySQL;

import utils.Logging;
import utils.Props;

/**
 * <p>
 * Java UNIX daemon test file.</p>
 * <p>
 * Title: TestSMSDaemon.java</p>
 * <p>
 * Description: This class is used to test the functionality of the Java
 * Daemon.</p>
 * <p>
 * Created on 21 March 2012, 10:48</p>
 * <hr />
 *
 * @since 1.0
 * @author Reuben Paul Wafula
 * @version Version 1.0
 */
@SuppressWarnings({"ClassWithoutLogger", "FinalClass"})
public final class TestSMSDaemon {

    /**
     * Logger for this application.
     */
    private static Logging log;
    /**
     * Loads system properties.
     */
    private static Props props;
    /**
     * The main processRequests class.
     */
    private static SendSMSDaemon sendSMSDaemon;
    /**
     * Instance of the MySQL connection pool.
     */
    private static MySQL mysql;

    /**
     * Private constructor.
     */
    private TestSMSDaemon() {
    }

    /**
     * Test init().
     */
    public static void init() {
        props = new Props();
        log = new Logging(props);

        try {
            mysql = new MySQL(props.getDbHost(), props.getDbPort(),
                    props.getDbName(), props.getDbUserName(),
                    props.getDbPassword(), props.getDbPoolName(),
                    props.getMaxConnections());
        } catch (ClassNotFoundException ex) {
            log.fatal(ex.getMessage());
        } catch (InstantiationException ex) {
            log.fatal(ex.getMessage());
        } catch (IllegalAccessException ex) {
            log.fatal(ex.getMessage());
        } catch (SQLException ex) {
            log.fatal(ex.getMessage());
        }

        sendSMSDaemon = new SendSMSDaemon(props, log, mysql);
    }

    /**
     * Main method.
     *
     * @param args command line arguments
     */
    @SuppressWarnings({"SleepWhileInLoop", "UseOfSystemOutOrSystemErr"})
    public static void main(final String[] args) {
        init();
        while (true) {
            try {
                log.info("");
                sendSMSDaemon.processRequests();
                Thread.sleep(props.getSleepTime());
            } catch (InterruptedException ex) {
                System.err.println(ex.getMessage());
            }
            //System.exit(0);
        }

    }
}
