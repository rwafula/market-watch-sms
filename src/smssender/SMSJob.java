package smssender;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import java.text.SimpleDateFormat;

import java.util.Date;

import db.MySQL;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import utils.Logging;
import utils.Props;
import org.json.*;
import utils.AfricasTalkingGateway;

/**
 * Creates a simple Runnable that holds a Job object.
 */
@SuppressWarnings("FinalClass")
public final class SMSJob implements Runnable {

    /**
     * MySQL connection object.
     */
    private transient MySQL mysql;
    /**
     * Logger instance.
     */
    private transient Logging logger;
    /**
     * Outbound ID.
     */
    private transient int outboundID = 0;
    /**
     * The source address.
     */
    private transient String sourceAddress = "SMSLEOPARD";
    /**
     * The destination address.
     */
    private transient String destinationAddress;
    /**
     * The message.
     */
    private transient String message;
    

    /**
     * Message ID.
     */
    private transient int messageID = 0;
    private transient int numberOfSends = 0;
    /**
     * Properties file
     */
    private transient Props props;
    
    

    /**
     * Creates a simple Runnable that holds a Job object.
     *
     
     */
    public SMSJob(final MySQL mysqlPool, final Logging log, 
            final String msisdn, final String messageContent, final int numberOfSends, int messageID,
            final String sourceAddr, final Props props) {
        mysql = mysqlPool;
        logger = log;
        destinationAddress = msisdn;
        message = messageContent;
        this.numberOfSends = numberOfSends;

        this.messageID = messageID;
        this.sourceAddress = sourceAddr;
        this.props = props;
        

    }

    /**
     * Starts the job.
     */
    @Override
    public void run() {
        
        logger.info(Thread.currentThread().getName() + " : Running SMSJob via route: "+props.getRoute());
        if (props.getRoute() == 1){
            sendXMLSMS();
        }else if (props.getRoute() ==2){        
            sendAfricaIsTakingSMS();
        }else if (props.getRoute() ==3){
            logger.info("Selecting route 3");
            sendGSMSMS();
        }else{
            sendAfricaIsTakingSMS();
        }
        
        
    }

    /**
     * Send sms via info beep api
     */
    private String prepareGETSMS(String getUrl, String username, String password, String sender) {
        String content = message;
        try {
            content = URLEncoder.encode(this.message, "UTF-8");
        } catch (Exception e) {
            logger.info(Thread.currentThread().getName() + " : Error trying to encode"
                    + " message :: " + message + "\n " + e.getMessage());
        }


        String url = getUrl + "?user=" + username + "&"
                + "password=" + password + "&sender=" + sender + "&SMSText=" + content
                + "&GSM=" + this.destinationAddress + "&type=longSMS&messageId=" + this.messageID;

        logger.info(Thread.currentThread().getName() + " : Generated GET URl: " + url);

        return url;
    }

    private String prepareXMLSMS(String username,
            String password, String sender) {

        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            // root elements
            Document doc = docBuilder.newDocument();
            Element smsRoot = doc.createElement("SMS");
            doc.appendChild(smsRoot);

            // auth elements
            Element auth = doc.createElement("authentication");


            // username elements
            Element usernameEl = doc.createElement("username");
            usernameEl.appendChild(doc.createTextNode(username));
            auth.appendChild(usernameEl);

            // Pass elements
            Element passwordEl = doc.createElement("password");
            passwordEl.appendChild(doc.createTextNode(password));
            auth.appendChild(passwordEl);

            smsRoot.appendChild(auth);

            // Message elements
            Element mesageEL = doc.createElement("message");

            //sender element
            Element senderEl = doc.createElement("sender");
            senderEl.appendChild(doc.createTextNode(sender));
            mesageEL.appendChild(senderEl);

            Element textEl = doc.createElement("text");
            textEl.appendChild(doc.createTextNode(message));
            mesageEL.appendChild(textEl);

            smsRoot.appendChild(mesageEL);

            //sender receipients
            Element recipientsEl = doc.createElement("recipients");

            //mobile number
            Element receiverEL = doc.createElement("gsm");
            receiverEL.appendChild(doc.createTextNode(this.destinationAddress));
            receiverEL.setAttribute("messageId", "" + this.messageID);
            recipientsEl.appendChild(receiverEL);



            smsRoot.appendChild(recipientsEl);

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new StringWriter());


            // generate String for output
            // StreamResult result = new StreamResult(System.out); 
            transformer.transform(source, result);

            String outString = result.getWriter().toString();

            outString = outString.substring(outString.indexOf("?>") + 2);

            logger.info(Thread.currentThread().getName() + " : Generated XML STRING: " + outString);
            return outString;

        } catch (ParserConfigurationException pce) {
            logger.info(Thread.currentThread().getName() + " : ParserConfigurationException error : "
                    + pce.getMessage());
            pce.printStackTrace();

        } catch (TransformerException tfe) {
            logger.info(Thread.currentThread().getName() + " : TransformerException error : "
                    + tfe.getMessage());
            tfe.printStackTrace();
        }


        return null;

    }

    private void sendSimpleSMS() {

        String connectionUrl = "";

        try {
            message = URLEncoder.encode(message, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            logger.error(Thread.currentThread().getName()
                    + " : Error encoding message: " + ex.getMessage());
        }

        try {
            // Experimental, its supposed to handle the text SOA scenario
            sourceAddress = URLEncoder.encode(sourceAddress, "UTF-8");
        } catch (UnsupportedEncodingException ex) {
            logger.error(Thread.currentThread().getName()
                    + " : Error encoding source address: " + ex.getMessage());
        }

        connectionUrl = prepareGETSMS(props.getGETSmsURL(), props.getInfoBipUsername(),
                props.getInfoBipPassword(), props.getInfoBipSender());

        logger.info(Thread.currentThread().getName() + " : " + connectionUrl);
        logger.info(Thread.currentThread().getName() + " : Read sms Url start");

        String smsResults = null;
        try {
            smsResults = readURL(connectionUrl);
        } catch (IOException ex) {
            logger.error(Thread.currentThread().getName() + " : "
                    + ex.getMessage());
        }

        logger.info(Thread.currentThread().getName() + " : Read sms Url "
                + "finished. Starting update of outbound");

    }

    /**
     * This send xml request to infobip
     */
    private void sendXMLSMS() {


        logger.info(Thread.currentThread().getName() + " : Read xml sms Url start");

        String smsResults = null;
        try {
            smsResults = postURL();
            ArrayList<Map<String, String>> results = extractResults(smsResults);

            for (Map<String, String> resultMap : results) {

                updateTransaction(resultMap.get("status"), resultMap.get("messageId"),
                        resultMap.get("destination"));
            }


        } catch (IOException ex) {
            logger.error(Thread.currentThread().getName() + " : "
                    + ex.getMessage());
        } catch (Exception ex) {
            logger.error(Thread.currentThread().getName() + " : "
                    + ex.getMessage());
        }

        logger.info(Thread.currentThread().getName() + " : Read sms Url "
                + "finished. Starting update of outbound");


    }
    
    // Make sure the downloaded jar file is in the classpath


    private void sendAfricaIsTakingSMS()
    {
	
	// Specify the numbers that you want to send to in a comma-separated list
	// Please ensure you include the country code (+254 for Kenya in this case)
	String recipients = this.destinationAddress.startsWith("+")? 
                this.destinationAddress:"+"+this.destinationAddress;
	
	// And of course we want our recipients to know what we really do
	
	// Create a new instance of our awesome gateway class
	AfricasTalkingGateway gateway  = new AfricasTalkingGateway(
                props.getAfricasTalkingUsername(), props.getAfricasTalkingKey());
	
	// Thats it, hit send and we'll take care of the rest. Any errors will
	// be captured in the Exception class below
	try {
            
            
	    JSONArray results = gateway.sendMessage(recipients, this.message, "EDAIRY");
	    for( int i = 0; i < results.length(); ++i ){
		JSONObject result = results.getJSONObject(i);
		//System.out.print(result.getString("status") + ",");
		//System.out.print(result.getString("number") + ",");
		//System.out.print(result.getString("messageId") + ",");
		//System.out.println(result.getString("cost"));
                logger.info(Thread.currentThread().getName() + "Found results: "
                        + "Status=>: "+result.getString("status") + ", "
                        + "Number=>: "+result.getString("number")+ ", "
                        + "Message Id=>: "+result.getString("messageId") + ", "
                        + "Cost=>: "+result.getString("cost")  +" "                        
                        + " From Africa is talking network.");
                
                 int statusF = 3;
                
                 if (result.getString("status").equalsIgnoreCase("Success"))
                     statusF = 1;
                
                 updateTransaction(""+statusF, result.getString("messageId"),
                        result.getString("number"));
	    }
	} catch (Exception e) {
            logger.info(Thread.currentThread().getName() + " :Encountered an error while sending "
                    + " over Africa is talking network."+e.getMessage());

	    //System.out.println("Encountered an error while sending " + e.getMessage());
	}
	
    }

    
    
    private void sendGSMSMS(){
        
        logger.info(Thread.currentThread().getName() +" SendGSMSMS ... calling Gatewy class"); 
        String recipients = this.destinationAddress.startsWith("+")? 
                this.destinationAddress:"+"+this.destinationAddress;
        
        SendGSMSMS sender = new SendGSMSMS(props, recipients, message, logger);
        
        logger.info(Thread.currentThread().getName() +" SMS Sender class  created ... sender.doIT()"); 
        int status = 3; //Assume failed until sent
        try{
            if(sender.doIt()){
                status = 1;
            }
        }
        catch (Exception e)
        {
            //Got trouble never send
            status = 3;
            logger.info(Thread.currentThread().getName() +" Error SMS Sender.. sender.doIT() \n" + e.getMessage()); 
            e.printStackTrace();
            
        }
        
        updateTransaction(""+status, null,
                        recipients);
    }

    /**
     * This function will help us parse xml responses and find return values
     *
     * @param xml
     * @return
     * @throws Exception
     */
    public static Document readXMLFromString(String xml) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        return builder.parse(is);
    }

    /**
     * Connects to the specified url and reads the response.
     *
     * @param uri the url
     *
     * @return the response message
     *
     * @throws IOException on error
     */
    @SuppressWarnings("NestedAssignment")
    private String readURL(final String uri)
            throws IOException {
        URL url = new URL(uri);
        HttpURLConnection hsurl = (HttpURLConnection) url.openConnection();
        hsurl.setConnectTimeout(10000);
        hsurl.setReadTimeout(30000);
        hsurl.setInstanceFollowRedirects(false);
        hsurl.setRequestMethod("GET");
        hsurl.setRequestProperty("Content-Type", "text/plain");
        hsurl.setRequestProperty("charset", "utf-8");
        hsurl.connect();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(hsurl.getInputStream()));

        String inputLine;
        String string = "";

        if (hsurl.getResponseCode() == 200) {
            while ((inputLine = in.readLine()) != null) {
                string += (inputLine + "~");
            }
        } else {
            logger.fatal(Thread.currentThread().getName() + " : STATUS CODE: "
                    + hsurl.getResponseCode() + " on url call " + uri);
        }

        in.close();

        logger.info(Thread.currentThread().getName() + " : Returned String = "
                + string);

        return string;
    }

    private String postURL() throws Exception {

        HttpClient client = new DefaultHttpClient();
        HttpPost post = new HttpPost(props.getXMLSmsURL());

        // add header
        //post.setHeader("User-Agent", USER_AGENT);

        List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();

        String xml = prepareXMLSMS(props.getInfoBipUsername(),
                props.getInfoBipPassword(), props.getInfoBipSender());

        urlParameters.add(new BasicNameValuePair("XML", xml));


        post.setEntity(new UrlEncodedFormEntity(urlParameters));

        HttpResponse response = client.execute(post);

        logger.info(Thread.currentThread().getName() + "\nSending 'POST' request to URL : "
                + props.getXMLSmsURL());


        logger.info(Thread.currentThread().getName() + " Post parameters : " + post.getEntity());
        logger.info(Thread.currentThread().getName() + "Response Code : "
                + response.getStatusLine().getStatusCode());

        BufferedReader rd = new BufferedReader(
                new InputStreamReader(response.getEntity().getContent()));

        StringBuilder result = new StringBuilder();

        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }

        logger.info(Thread.currentThread().getName() + "Final Resulsts : "
                + result.toString());

        return result.toString();
    }

    /**
     * Sends the message.
     */
    private void updateTransaction(String stat, String infobipId, String destination) {


        int status = 3;
        try {
            status = Integer.parseInt(stat);
            /* infobip will return a zero on success - our success is 1; */
            if (status == 0) {
                status = 1; //success
            }
        } catch (Exception e) {
        }

        String trxSql = "";
        if(props.getRoute() ==1){
            trxSql = "update out_message set status = '" + status + "', last_send = now(), "
                + " number_of_sends = '" + (numberOfSends + 1) + "'"
                + " where id = '" + this.messageID + "'";
        }else{
               trxSql = "update out_message set status = '" + status + "', last_send = now(), "
                + " number_of_sends = '" + (numberOfSends + 1) + "'"
                + " where id = '" + this.messageID + "'";
        }


        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;

        try {
            conn = mysql.getConnection();
            stmt = conn.createStatement();

            stmt.executeUpdate(trxSql);


        } catch (SQLException ex) {
            logger.error(Thread.currentThread().getName()
                    + " | sendMessageDirectly --- Failed to send message: "
                    + ex.getMessage());

        } finally {
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | sendMessageDirectly --- Failed to close "
                            + "Statement object. Error: "
                            + ex.getMessage());
                }
            }

            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException ex) {
                    logger.fatal(Thread.currentThread().getName()
                            + " | sendMessageDirectly --- Failed to close "
                            + "connection object. Error: "
                            + ex.getMessage());
                }
            }
        }
    }

    /**
     * Get the time. This returns the time in the format of 'hh:mm:ss'.
     *
     * @return the current time
     */
    private String getCurrentTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        Date date = new Date();
        return sdf.format(date);
    }

    /**
     * Return the date plus time.
     *
     * @return the date plus time
     */
    private String getDateTime() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        return sdf.format(date);
    }

    private ArrayList<Map<String, String>> extractResults(String smsResults)
            throws Exception {

        ArrayList<Map<String, String>> results = new ArrayList<Map<String, String>>();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        DocumentBuilder db = dbf.newDocumentBuilder();
        ByteArrayInputStream bis = new ByteArrayInputStream(smsResults.getBytes());
        Document doc = db.parse(bis);

        doc.getDocumentElement().normalize();
        logger.info("Root element :" + doc.getDocumentElement().getNodeName());

        NodeList nList = doc.getElementsByTagName("result");
        logger.info("----------------------------");

        Map<String, String> resultMap = new HashMap<String, String>();

        for (int temp = 0; temp < nList.getLength(); temp++) {

            Node nNode = nList.item(temp);
            logger.info("\nCurrent Element :" + nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;
                String status = eElement.getElementsByTagName("status").item(0).getTextContent();
                resultMap.put("status", status);
                logger.info("status : " + status);

                String messageId = eElement.getElementsByTagName("messageid").item(0).getTextContent();
                resultMap.put("messageId", messageId);
                logger.info("messageID : " + messageId);

                String destination = eElement.getElementsByTagName("destination").item(0).getTextContent();
                resultMap.put("destination", destination);
                logger.info("destination : " + destination);

                results.add(resultMap);
            }
        }

        return results;


    }

    public static void main(String[] args) throws Exception {

        String smsResults = "<results>"
                + "            <result>"
                + "            <status>0</status>"
                + "                  <messageid>59</messageid>"
                + "                   <destination>254726986944</destination>"
                + "              </result>"
                + "             </results>";

    }
}
