/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package smssender;

/**
 *
 * @author rube
 */
import java.io.IOException;
import org.smslib.AGateway;
import org.smslib.IOutboundMessageNotification;
import org.smslib.Library;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;
import utils.Logging;
import utils.Props;

public class SendGSMSMS {

    private final String GSMid;
    private final String GSMComPort;
    private final int GSMBoudRate;
    private final String GSMManufacturer;
    private final String GSMModel;
    private final String SIMPin;
    private final String SMSSNumber;
    private Service service;

    private final String to;
    private final String message;
    private transient Logging logger;

    public SendGSMSMS(Props props, String to, String message, Logging logger) {
        this.GSMid = props.getGSMid();
        this.GSMComPort = props.getGSMComPort();
        this.GSMBoudRate = props.getGSMBoudRate();
        this.GSMManufacturer = props.getGSMManufacturer();
        this.GSMModel = props.getGSMModel();
        this.SIMPin = props.getSIMPin();
        this.SMSSNumber = props.getSMSCNumber();
        this.to = to;
        this.message = message;
        this.logger = logger;

        this.logger.info(Thread.currentThread().getName() + " SendGSMSMS ID " + this.GSMid + " ..Port: " + this.GSMComPort
                + "Rate " + this.GSMBoudRate + " Manu " + this.GSMManufacturer
                + " Model " + this.GSMModel + " SIM " + this.SIMPin);
    }

    public boolean doIt() {
        
        try {
            OutboundNotification outboundNotification = new OutboundNotification();
            this.logger.info(Thread.currentThread().getName() + " Calling send message from GSM Modem");

            System.out.println(Library.getLibraryDescription());
            System.out.println("Version: " + Library.getLibraryVersion());
            SerialModemGateway gateway = new SerialModemGateway(
                    GSMid, GSMComPort, GSMBoudRate, GSMManufacturer, GSMModel);
            gateway.setInbound(true);
            gateway.setOutbound(true);
            gateway.setSimPin(SIMPin);
            // Explicit SMSC address set is required for some modems.
            // Below is for VODAFONE GREECE - be sure to set your own!--- i set my own
            gateway.setSmscNumber(this.SMSSNumber);
            this.logger.info(Thread.currentThread().getName() + " Creating send SMS service");
            service = new Service();
            service.setOutboundNotification(outboundNotification);
            service.addGateway(gateway);
            service.startService();
            this.logger.info(Thread.currentThread().getName() + " Service Started ");
            System.out.println();
            System.out.println("Modem Information:");
            System.out.println("  Manufacturer: " + gateway.getManufacturer());
            System.out.println("  Model: " + gateway.getModel());
            System.out.println("  Serial No: " + gateway.getSerialNo());
            System.out.println("  SIM IMSI: " + gateway.getImsi());
            System.out.println("  Signal Level: " + gateway.getSignalLevel() + " dBm");
            System.out.println("  Battery Level: " + gateway.getBatteryLevel() + "%");
            System.out.println();
            // Send a message synchronously.
            OutboundMessage msg = new OutboundMessage(to, message);
            service.sendMessage(msg);
            this.logger.info(Thread.currentThread().getName() + " Message sent stopping service to "
                    + to);
            System.out.println(msg);
            service.stopService();
            
            return true;
            
        } catch (Exception exe) {
            this.logger.info(Thread.currentThread().getName() + " Exception in send message diIt() "
                    + exe.getMessage());
            
            return false;
        } finally {
            try {              
                service.stopService();
            } catch (Exception ex) {
                this.logger.info(Thread.currentThread().getName() + " Error stopping send SMS service "
                        + ex.getMessage());
            }
        }
      
    }

    public class OutboundNotification implements IOutboundMessageNotification {

        public void process(AGateway gateway, OutboundMessage msg) {
            System.out.println("Outbound handler called from Gateway: " + gateway.getGatewayId());
            System.out.println(msg);
        }

        @Override
        public void process(String string, OutboundMessage msg) {
            System.out.println("Outbound handler called from Gateway: " + string);
            System.out.println(msg);

        }
    }
}
