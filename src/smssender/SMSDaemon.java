package smssender;

import java.sql.SQLException;

import db.MySQL;

import utils.Logging;
import utils.Props;

import org.apache.commons.daemon.Daemon;
import org.apache.commons.daemon.DaemonContext;
import org.apache.commons.daemon.DaemonController;
import org.apache.commons.daemon.DaemonInitException;

/*
 * @since 1.0
 * @author Reuben Paul Wafula
 * @version Version 1.0
 */
@SuppressWarnings({"ClassWithoutLogger", "FinalClass"})
public final class SMSDaemon implements Daemon, Runnable {

    /**
     * The worker thread that does all the work.
     */
    private transient Thread worker;
    /**
     * Flag to check if the worker thread should processRequests.
     */
    private transient boolean working = false;
    /**
     * Logger for this application.
     */
    private transient Logging log;
    /**
     * Loads system properties.
     */
    private transient Props props;
    /**
     * The main processRequests class.
     */
    private transient SendSMSDaemon sendSMSDaemon;
    /**
     * Instance of the MySQL connection pool.
     */
    private transient MySQL mysql;

    /**
     * Used to configuration files, create a trace file, create ServerSockets,
     * Threads, etc.
     *
     * @param context the DaemonContext
     *
     * @throws DaemonInitException on error
     */
    @Override
    public void init(final DaemonContext context) throws DaemonInitException {
        worker = new Thread(this);

        props = new Props();

        log = new Logging(props);
        log.info("Initializing MD daemon...");

        try {

            mysql = new MySQL(props.getDbHost(), props.getDbPort(),
                    props.getDbName(), props.getDbUserName(),
                    props.getDbPassword(), props.getDbPoolName(),
                    props.getMaxConnections());

        } catch (ClassNotFoundException ex) {
            log.fatal(ex.getMessage());
        } catch (InstantiationException ex) {
            log.fatal(ex.getMessage());
        } catch (IllegalAccessException ex) {
            log.fatal(ex.getMessage());
        } catch (SQLException ex) {
            log.fatal(ex.getMessage());
        }

        sendSMSDaemon = new SendSMSDaemon(props, log, mysql);
    }

    public void init() throws DaemonInitException {
        worker = new Thread(this);

        props = new Props();

        log = new Logging(props);
        log.info("Initializing MD daemon...");

        try {

            mysql = new MySQL(props.getDbHost(), props.getDbPort(),
                    props.getDbName(), props.getDbUserName(),
                    props.getDbPassword(), props.getDbPoolName(),
                    props.getMaxConnections());

        } catch (ClassNotFoundException ex) {
            log.fatal(ex.getMessage());
        } catch (InstantiationException ex) {
            log.fatal(ex.getMessage());
        } catch (IllegalAccessException ex) {
            log.fatal(ex.getMessage());
        } catch (SQLException ex) {
            log.fatal(ex.getMessage());
        }

        sendSMSDaemon = new SendSMSDaemon(props, log, mysql);
    }

    /**
     * Starts the daemon.
     */
    @Override
    public void start() {
        working = true;
        worker.start();
        log.info("Starting MD daemon...");
    }

    public static void main(String args[]) {
        SMSDaemon daemon = new SMSDaemon();
        try {
            daemon.init();
            daemon.start();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Failed to start service");
        }
    }

    /**
     * Stops the daemon. Informs the thread to terminate the processRequests().
     */
    @Override
    @SuppressWarnings("SleepWhileInLoop")
    public void stop() {
        log.info("Stopping MarketWachSMSSender Scheduler daemon...");

        working = false;

        while (!sendSMSDaemon.getIsCurrentPoolShutDown()) {
            log.info("Waiting for current thread pool to complete tasks...");
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                log.error("InterruptedException occured while waiting for "
                        + "tasks to complete: " + ex.getMessage());
            }
        }

        log.info("Completed tasks in current thread pool, continuing daemon "
                + "shutdown");

        try {
            log.info("Shutting down MySQL connection pool...");
            mysql.shutdownDriver();
            log.info("Daemon stopped.");
        } catch (SQLException ex) {
            log.error("Error shutting down MySQL connection pool: "
                    + ex.getMessage());
        }
    }

    /**
     * Destroys the daemon. Destroys any object created in init().
     */
    @Override
    public void destroy() {
        log.info("Destroying MD daemon...");
        log.info("Exiting...");
    }

    /**
     * Runs the thread. The application runs inside an "infinite" loop.
     */
    @Override
    @SuppressWarnings({"SleepWhileHoldingLock", "SleepWhileInLoop"})
    public void run() {
        while (working) {
            try {
                log.info("");
                log.info("");

                //spawn multiple two prcesses send and receive SMS
                sendSMSDaemon.processRequests();

                Thread.sleep(props.getSleepTime());
            } catch (InterruptedException ex) {
                log.error("InterruptedException occured: " + ex.getMessage());
            } catch (Exception ex) {
                log.error("General error occured: " + ex.getMessage());
            }

        }
    }
}
