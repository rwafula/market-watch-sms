#! /bin/bash

# Directory where the application resides
DIR=~/apps/JamboPayExpress/umojapay/service/daemons/SMSsender
# Application JAR file (may be in a sub folder)
JAR_FILE=$DIR/SMSsender.jar
# The PID file location
PID=/var/run/smssender.pid
# JVM in use
JAVA_HOME=/usr/lib/jvm/java-7-openjdk-i386
# Name of the daemon (will be displayed durning start/stop)
NAME="SMSsender"
# Main class implementing the Daemon interface
MAIN_CLASS=smssender.SMSDaemon
# Logging properties file
LOG_PROPERTIES_FILE=$DIR/logging.properties

MIN_MEMORY=-Xms256m
MAX_MEMORY=-Xmx1024m

# You can enable a security policy if you need it here
#SECURITY_POLICY="-Djava.security.manager -Djava.security.policy=$DIR/daemon.policy"
SECURITY_POLICY=

# Set to 1 to enable debugging
DEBUG=1
DEBUG_OUTPUT_FILE=/apps/JamboPayExpress/umojapay/service/daemons/logs/debug.log
DEBUG_ERROR_FILE=/apps/JamboPayExpress/umojapay/service/daemons/logs/debug_error.log


# DO NOT EDIT BELOW THIS LINE

usage() {
	echo $"Usage: $0 {start|stop|restart} "
	return 0
}

start() {
    echo $"Starting the $NAME..."

    cd $DIR

    if [[ $DEBUG -eq 1 ]]; then
        /usr/bin/jsvc -debug -pidfile $PID -home $JAVA_HOME $SECURITY_POLICY -Djava.util.logging.config.file=$LOG_PROPERTIES_FILE -outfile $DEBUG_OUTPUT_FILE -errfile $DEBUG_ERROR_FILE $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    else
        /usr/bin/jsvc -pidfile $PID -home $JAVA_HOME $SECURITY_POLICY -Djava.util.logging.config.file=$LOG_PROPERTIES_FILE $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    fi

    # Check status of the application
    if [[ $? -eq 0 ]]; then
        echo $"$NAME Successfully STARTED"
        echo
        return 0
    else
        echo $"Failed to START $NAME"
        echo
        return 1
    fi
}

stop() {
    echo $"Stopping the $NAME..."

    cd $DIR

    if [[ $DEBUG -eq 1 ]]; then
        /usr/bin/jsvc -debug -stop -home $JAVA_HOME -pidfile $PID $SECURITY_POLICY -Djava.util.logging.config.file=$LOG_PROPERTIES_FILE -outfile $DEBUG_OUTPUT_FILE -errfile $DEBUG_ERROR_FILE $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    else
        /usr/bin/jsvc -stop -home $JAVA_HOME -pidfile $PID $SECURITY_POLICY -Djava.util.logging.config.file=$LOG_PROPERTIES_FILE $MIN_MEMORY $MAX_MEMORY -cp $JAR_FILE $MAIN_CLASS
    fi

    if [[ -e $PID ]]; then
        # Kill the process (so we are sure that it has stopped)
        KPID=`cat $PID`
        KPID1=$(($KPID - 1))
        kill -9 $KPID $KPID1
        rm -f $PID
    fi

    # Check status of the application
    if [[ $? -eq 0 ]]; then
        echo $"$NAME Successfully STOPPED"
        echo
        return 0
    else
        echo $"Failed to STOP $NAME"
        echo
        return 1
    fi
    echo
}

restart() {
    cd $DIR

    stop

    sleep 10

    if [[ -e $PID ]]; then
        # Kill the process (so we are sure that it has stopped)
        KPID=`cat $PID`
        KPID1=$(($KPID - 1))
        kill -9 $KPID $KPID1
        rm -f $PID
    fi

    sleep 2

    start

    # Check status of the application
    if [[ $? -eq 0 ]]; then
        echo $"$NAME Successfully RESTARTED"
        echo
        return 0
    else
        echo $"Failed to RESTART $NAME"
        echo
        return 1
    fi

    echo
}

case "$1" in
    start)
        start
    ;;
    stop)
        stop
    ;;
    restart)
        restart
    ;;
    *)
        echo $"Usage: $0 {start|stop|restart}"
        exit 1

esac

exit $?

